import './env'
import Server from './server'

const start = async () => {

  const server = await Server()
  await server.start()
  console.log(`Server running on ${server.info.uri}`)
}

start().catch(console.error)

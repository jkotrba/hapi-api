# common base image for dev and production
FROM node:10.16-alpine AS BASE

RUN apk add --no-cache bash

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.5.1/wait /wait
RUN chmod +x /wait

# set working directory
WORKDIR /app

# install and cache app dependencies
COPY package*.json ./
RUN npm install

# Copy the rest of the files
COPY . .

CMD /wait && npm start

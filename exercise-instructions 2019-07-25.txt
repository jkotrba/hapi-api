[
  {
    id: 'cc313ebb-4556-4c74-94fb-66041b788420',
    email: 'blahblah@email.com',
    handle: 'blah-blah'
  },
  {
    id: 'ae1a2e2b-ad44-43b1-a0af-7696c67bc25c',
    email: 'nother_guy@gmail.com',
    handle: 'nother_guy'
  },
  {
    id: '93704b3a-f3aa-4528-91e6-52362692719d',
    email: 'oldman@aol',
    handle: 'grandpa4592'
  }
]

import uuid from 'uuid/v4'


GET /users
GET /users/2
POST /users

{
  email: '',
  handle: ''
}

200 OK

{
  id: '0a037256-6192-42d1-b119-27d50c1d2598'
}
